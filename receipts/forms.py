from django import forms
from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account

class ReceiptCreateForm(ModelForm):
    class Meta:
        model= Receipt
        fields=[
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
            
            
        ]

class CategoryCreateForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
            
        ]

class AccountCreateForm(ModelForm):
    class Meta:
        model=Account
        fields=[
            "name",
            "number",
        ]
