from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptCreateForm , CategoryCreateForm, AccountCreateForm
# Create your views here.

@login_required
def receipt_list(request):
    receipts= Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method=="POST":
        form = ReceiptCreateForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
        return redirect ("home")
    else:
        form = ReceiptCreateForm()
    context = {
     "form":form,
    }
    return render(request,"receipts/create.html", context)


@login_required
def expense_category(request):
    category =ExpenseCategory.objects.filter(owner=request.user)
    context ={
        "category": category,
    }
    return render(request, "receipts/category.html", context)

@login_required
def accounts(request):
    accounts =Account.objects.filter(owner=request.user)
    context ={
        "accounts":accounts
    }
    return render(request, "receipts/accounts.html", context)

@login_required
def create_category(request):
    if request.method=="POST":
        print(request)
        form=CategoryCreateForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner=request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryCreateForm()
    context={
        "form": form,
    }
    return render(request, "receipts/new_category.html", context)

@login_required
def create_account(request):
    if request.method=="POST":     
        form=AccountCreateForm(request.POST)
        if form.is_valid():
            account= form.save(False)
            account.owner=request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountCreateForm()
    context={
        "form":form,
    }
    return render(request, "receipts/new_account.html", context)
